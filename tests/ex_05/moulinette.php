<?php

require_once($argv[1]);

$arthur = new Gecko("Arthur", 22);
$anom = new Gecko();
$benjy = new Gecko("Benjy");

unset($benjy);

echo $arthur->getName();
echo $anom->getName() . "\n";

echo $arthur->getAge() . "\n";
for($i = -1; $i <= 14; ++$i){
    $anom->setAge($i);
    $anom->status();
}    

$anom->hello("Titi");
$anom->hello(5);

$arthur->hello("Billy");
$arthur->hello(3);
$arthur->hello(-23);
