<?php

class Snake
{
};

require_once($argv[1]);

$arthur = new Gecko("Arthur", 22);
$anom = new Gecko();
$benjy = new Gecko("Benjy");

unset($benjy);

echo $arthur->getName();
echo $anom->getName() . "\n";

echo $arthur->getAge() . "\n";
for($i = -1; $i <= 14; ++$i){
    $anom->setAge($i);
    $anom->status();
}    

$anom->hello("Titi");
$anom->hello(5);

$arthur->hello("Billy");
$arthur->hello(3);
$arthur->hello(-23);

echo $anom->getEnergy() . "\n";
$anom->setEnergy(-1);
echo $anom->getEnergy() . "\n";
$anom->setEnergy(101);
echo $anom->getEnergy() . "\n";

$arthur->eat("meat");
$arthur->eat("MeAt");
$arthur->eat("MeAtoooo");
echo $arthur->getEnergy() . "\n";
$arthur->eat("VEGETABLE");
$arthur->eat("VEgETaBLE");
echo $arthur->getEnergy() . "\n";
$arthur->work();
echo $arthur->getEnergy() . "\n";
$arthur->setEnergy(0);
$arthur->eat("vegetable");
echo $arthur->getEnergy() . "\n";
$arthur->work();
echo $arthur->getEnergy();
echo $arthur->setEnergy(25) . "\n";
$arthur->work();
echo $arthur->getEnergy() . "\n";
$arthur->work();
echo $arthur->getEnergy() . "\n";
