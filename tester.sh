#!/bin/bash

exercise=ex_$1
binary=Gecko.php
output="output.test"

function play_mouli
{
if [[ $# -eq 2 ]] && [[ $2 -eq "-c" ]]
then
    cd ./tests/$exercise && php moulinette.php $binary &> "../${exercise}_REF"
else
    sudo -u student php ./tests/$exercise/moulinette.php ./$exercise/$binary &> output.test
    res=$(diff -a <(cat -e "./tests/${exercise}_REF" ) <(cat -e output.test))

    if [ $? -ne 0 ]; then
	echo "KO: check your traces below this line..."
	echo "$res"
    else
	echo "OK"
    fi
fi
}

if [ $# -lt 1 ]
then
    echo -e "[ERREUR] $0 : pas assez d'arguments"
    echo -e "USAGE : $0 exercice [-c]"
    exit 2
fi

if [ ! -e $exercise ]
then
    echo "KO: exercise [$binary] not found"
    exit 2
fi

play_mouli $@
